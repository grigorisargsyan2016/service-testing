const EventTypes = {
    IN: 'in',
    OUT: 'out'
}

module.exports = {
    EventTypes
}