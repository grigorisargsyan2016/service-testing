const { EventTypes } = require("./config");

function testService(events) {
    if(!events.length) return true
    
    if(events.length % 2) return false

    const queue = {}

    for (let index = 0; index < events.length; index++) {
        const [name, event] = events[index];

        if (!queue[name]) {
            if(event === EventTypes.OUT) return false

            queue[name] = event
            continue;
        }

        if(event === EventTypes.IN) return false

        delete queue[name]
    }

    if(Object.keys(queue).length !== 0 ) return false

    return true
}

module.exports = testService
